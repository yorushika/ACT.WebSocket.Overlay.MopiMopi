# ACT.WebSocket.Overlay.MopiMopi Chinese Version

![pipeline status](https://gitlab.com/yorushika/yorushika.gitlab.io/badges/master/pipeline.svg)

MopiMopi overlay for plugin "WebSocket" of Advanced Combat Tracker, translated into Chinese with some function optimized.

## Basic Info

URL: <https://yorushika.gitlab.io/ACT.WebSocket.Overlay.MopiMopi/>

Original Author: HAERU

Translation(Chinese)/Optimization: [Yorushika](mailto:jeremiahshi@outlook.com)

## Warning

The slash line at the end of URL is **NECESSARY**!

Which means your URL in WebSocket should be something like **this**:

<https://yorushika.gitlab.io/ACT.WebSocket.Overlay.MopiMopi/?HOST_PORT=ws://127.0.0.1:10501/>

Instead of:

<https://yorushika.gitlab.io/ACT.WebSocket.Overlay.MopiMopi?HOST_PORT=ws://127.0.0.1:10501/>

### (Don't just copy the URLs above into your path directly! You may need to adjust the IP address and port to your own!)